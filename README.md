# SlimControl

Toolset to fast build [Slimphp](https://packagist.org/packages/slim/slim) REST api

## Installation

It's recommended that you use [Composer](https://getcomposer.org/) to install SlimControl.

```bash
$ composer require loandbeholdru/slimcontrol
```

## TODO / in progress

Package include many functions which i use more than 5 years, but this README so poor.  
Documentation in progress ... 

## Usage

### Fast build controller
```php
class postreport extends controllerApi
{
    const IN = ["report", "key", "data"];
    const ASSERT = ["report", "key" => "keycheck"];
    const DEFAULT = ["report" => "setdef"]


    protected function process()
    {
        // You can do it because all opts are checked
        extract($this->args);

        return $this->replyjson(compact('report', 'key', 'data'));
    }
    // assert key
    protected function keycheck($parameter, $valifexist){
        return $valifexist === "notwrongkey";
    }
    // set default val of report if it's absent
    protected function setdef($parameter){
        return "default val of report";
    }
}
```

Instead of using constants, you can override 3 methods:
```php
//...
    protected function defs()
    {
        return static::DEFAULTS;
    }

    protected function ins()
    {
        return static::IN;
    }

    protected function asss() 
    {
        return static::ASSERT;
    }
...
```

### Simple reply for request    
```php
$this->replyjson($message, string $status = statuses::SUCCESS, int $code = 200)
```

Return follow with 200 OK:  
```json
{
  "message" : "...",
  "status" : "success"
}
```

Predefined statuses:  
```php
class statuses
{
    const SUCCESS = 'success';
    const ERROR = 'error';
    const AUTH_FAIL = 'auth_fail';
    const DB_ERROR = 'db_error';
    const REQUEST_ERROR = 'request_error';
    const DISK_ERROR = 'disk_error';
}
```

### Few interesting middlewares

... documentation to be continued

## Contributions  

... are always welcome. Many times it is useful to just point out a use case the author have not thought about or come across.


