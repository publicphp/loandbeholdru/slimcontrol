<?php

use loandbeholdru\shorts\arrays;
use RedBeanPHP\R;
return function ($opts = null){
    extract($creds = ($opts ?? arrays::ifDefined('', 'DB_OPTS', [])) + [
            'dsn' => '',
            'engine' =>'mysql',
            'dbname' => '',
            'host' => '',
            'user' => '',
            'pass' => '',
            'freez' => false,
            'models' => 'app\\Models\\',
            'cache' => __DIR__ . '/../cache/hash',
        ]);


    define( 'REDBEAN_MODEL_PREFIX', $models);

    $dsn = empty($port) ? "$engine:host=$host;dbname=$dbname" :
        "$engine:host=$host;port=$port;dbname=$dbname";

//    echo $dsn;
    R::setup($dsn, $user, $pass);
    $a = R::dispense('option');
    $a->apikey = arrays::deephash($creds);
    R::store($a);
    R::trash($a);
    R::freeze( false );
};

