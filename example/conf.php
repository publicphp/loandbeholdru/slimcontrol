<?php
if (preg_match(
    '/\.(?:png|jpg|jpeg|gif|ico|js|css|tiff|tif|ttf)$/', $_SERVER["REQUEST_URI"] ?? '')
) {
    return false;    // сервер возвращает файлы напрямую.
}

error_reporting(E_ERROR | E_PARSE);
require __DIR__ . '/../../../vendor/autoload.php';
require __DIR__ . '/confapp.php';
define('PHP_BIN', '/usr/local/bin/php');

define('APP_DEBUG', true);

define('CACHE', __DIR__ . '/../cache');
define('TWIG_TEMPLATES', __DIR__ . '/../app/src/twigs');
define('TWIG_CACHE', CACHE . '/twig');
define('SOAP_LOG', __DIR__ . '/../logs/soap.log');
define('DB_CONFIGURATOR', __DIR__ . '/confdb.php');

define('PATHS', [
    TWIG_CACHE
]);





include __DIR__ . '/containers.php';
include __DIR__ . '/middleware.php';
