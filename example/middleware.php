<?php
require __DIR__ . '/../../../vendor/autoload.php';

// Место для подключения общих middleware
use loandbeholdru\slimcontrol\middlewares\allowOrigin;
use loandbeholdru\slimcontrol\middlewares\authApi;
use loandbeholdru\slimcontrol\middlewares\mysql;

$app->add(new allowOrigin('Api-token'))
    ->add(new authApi());
