<?php

// Место для настройки контейнеров

$app = new \Slim\App(['settings' => [
    'displayErrorDetails' => true
]]);

$container = $app->getContainer();

// Register component on container
$container['view'] = function ($container) {


    $view = new \Slim\Views\Twig(TWIG_TEMPLATES, [
//        'cache' => \app\confs::TWIG_CACHE
        'cache' => false
    ]);
    $debug = true;
    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment(compact('debug') + $_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

    return $view;
};

foreach (PATHS as $PATH) if (!realpath($PATH))
    mkdir($PATH, 0777, true);
//$container['clientid'] = null;