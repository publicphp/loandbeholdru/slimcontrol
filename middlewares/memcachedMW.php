<?php


namespace loandbeholdru\slimcontrol\middlewares;


use loandbeholdru\slimcontrol\api\controllerApi;
use loandbeholdru\slimcontrol\api\dbstatuses;

/**
 * middleware-класс для подключения к MEMCACHED серверу
 * используется, как правило, в качестве основы для другого
 * класса по загрузки окружения исполнения из кэша и в кэш.
 *
 * Class memcachedMW
 * @package loandbeholdru\slimcontrol\middlewares
 */
class memcachedMW extends middlewareProcess
{
    const CACHER = 'cacher';
    const ERROR_INIT = dbstatuses::CACHE_CONNECTION_ERROR;

    protected \Memcached $mc;

    public function __construct(string $mchost = 'localhost', string $mcport = '11211')
    {
        $this->mc = controllerApi::$storage[static::CACHER] ??
            $this->initcacher($mchost, $mcport);
    }
    /**
     * Инициализирует клиент MEMCACHED
     *
     * @param string $mchost
     * @param string $mcport
     * @return mixed
     */
    protected function initcacher(string $mchost, string $mcport)
    {
        try{
            controllerApi::$storage[static::CACHER] = new \Memcached();
            controllerApi::$storage[static::CACHER]->addServer($mchost, (int)$mcport);

            $un = uniqid();
            controllerApi::$storage[static::CACHER]->add($un, 'test', 1);
            if (controllerApi::$storage[static::CACHER]->get($un) != 'test')
                throw new \Exception('You have to setup MEMCACHED server!');
        }catch (\Throwable $exception){
            $this->break($exception->getMessage(), static::ERROR_INIT, 503);
        }

        return controllerApi::$storage[static::CACHER];
    }

    protected function process($request, $response, $next)
    {
        return $this;
    }

    /**
     * Обрабатывает полученное значение и возвращает правильный сигнал MEMCACHED
     *
     * @param $mc
     * @param $key
     * @param null $value
     * @return bool
     */
    public function empty($mc, $key, &$value = null)
    {
        return !empty($value);
    }
}