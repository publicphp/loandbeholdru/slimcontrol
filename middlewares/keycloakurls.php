<?php


namespace loandbeholdru\slimcontrol\middlewares;


/**
 * СЕРВИС АДРЕСОВ RESTAPI KEYCLOAK
 * Класс предполагает наличие глобальных задекларированных констант:
 *      KC_SCHEME, KC_SERVER, KC_CONTEXT
 *      KC_REALM, KC_USERNAME, KC_PASSWORD
 *      KC_CLIENT, KC_CLIENT_SECRET
 *
 * Class keycloakurls
 * @package loandbeholdru\slimcontrol\middlewares
 */
class keycloakurls
{
    static function users() {
        return sprintf(
        "%s://%s/%s/admin/realms/%s/users",
        KC_SCHEME, KC_SERVER, KC_CONTEXT, KC_REALM
        );
    }
    static function login() {
        return sprintf(
        "%s://%s/%s/realms/%s/protocol/openid-connect/token",
        KC_SCHEME, KC_SERVER, KC_CONTEXT, KC_REALM
        );
    }
    static function refresh() {
        return static::login();
    }

}