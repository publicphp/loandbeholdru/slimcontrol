<?php


namespace loandbeholdru\slimcontrol\middlewares;


use loandbeholdru\slimcontrol\api\controllerApi;
use loandbeholdru\slimcontrol\api\dbstatuses;
use loandbeholdru\slimcontrol\api\headers;
use loandbeholdru\slimcontrol\api\statuses;
use loandbeholdru\slimcontrol\middlewares\middlewareBase;
use loandbeholdru\slimcontrol\middlewares\middlewareException;
use loandbeholdru\slimcontrol\middlewares\keycloakurls;
use loandbeholdru\shorts\arrays;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\Factory;
use React\Http\Browser;
use RingCentral\Psr7\Response;

/**
 * Класс middleware, реализует получение авторизации от KeyCloak-серевера
 * и кеширование полученных токенов в MEMCACHED.
 * Зависит от loandbeholdru\slimcontrol\api\headers и loandbeholdru\slimcontrol\middlewares\keycloakurls.
 * Класс предполагает наличие задекларированных глобальных констант:
 *      KC_SCHEME, KC_SERVER, KC_CONTEXT
 *      KC_REALM, KC_USERNAME, KC_PASSWORD
 *      KC_CLIENT, KC_CLIENT_SECRET
 *
 * Class keycloakMW
 * @package loandbeholdru\slimcontrol\middlewares
 */
class keycloakMW extends memcachedMW
{
    const ACCESS_TOKEN = 'keycloak_access_token';
    const REFRESH_TOKEN = 'keycloak_refresh_token';
    const TOKEN_TYPE = "keycloak_token_type";
    const NB_POLICY = "keycloak_not-before-policy";
    const SESSION_STATE = "keycloak_session_state";
    const SCOPE = "keycloak_scope";

    protected $loop;
    protected Browser $client;

    protected function process($request, $response, $next)
    {
        controllerApi::$storage[static::ACCESS_TOKEN] = $this->mc->get(
            static::ACCESS_TOKEN, [$this, 'refresh']
        );

        controllerApi::$storage[static::ACCESS_TOKEN] = empty(controllerApi::$storage[static::ACCESS_TOKEN]) ?
            $this->mc->get(static::ACCESS_TOKEN, [$this, 'login']) : controllerApi::$storage[static::ACCESS_TOKEN];

        if (!controllerApi::$storage[static::ACCESS_TOKEN])
            $this->break("Can't authorize to KeyCloak server!", statuses::AUTH_FAIL, 401);

        return $this;
    }

    /**
     * Запрашивает access_token и обрабатывает ответ от сервера авторизации,
     * логинясь по refresh_token
     *
     * @param $mc
     * @param $key
     * @param $value
     * @return bool
     */
    public function refresh($mc, $key, &$value)
    {
        $grant_type = "refresh_token";
        $refresh_token = $mc->get(static::REFRESH_TOKEN, [$this, 'empty']);

        if (!empty($refresh_token)){
            $this->initclient()->client->post(
                keycloakurls::refresh(), headers::FORM, http_build_query(array_merge(
                    compact('grant_type','refresh_token',), headers::KC_CLIENT_CREDS
                )))->then(
                    fn(ResponseInterface $response) => $value = $this->store($response), [$this, 'servererror']
            );

            $this->loop->run();
        }

        return $this->empty($mc, $key, $value);
    }

    /**
     * Запрашивает access_token и обрабатывает ответ от сервера авторизации,
     * логинясь по имени пользователя и паролю
     *
     * @param $mc
     * @param $key
     * @param $value
     * @return bool
     */
    public function login($mc, $key, &$value)
    {
        $grant_type = "password";

        $this->initclient()->client->post(
            keycloakurls::refresh(), headers::FORM, http_build_query(array_merge(
            headers::KC_ADMIN_CREDS, headers::KC_CLIENT_CREDS, compact('grant_type')
        )))->then(
            fn(ResponseInterface $response) => $value = $this->store($response), [$this, 'servererror']
        );

        $this->loop->run();
        return $this->empty($mc, $key, $value);
    }

    /**
     * Сохраняет данные ответа от авторизующего сервера в MEMCACHED
     *
     * @param Response $response
     * @return $this|mixed|null
     */
    public function store(Response $response)
    {
        $data = arrays::valid_json(
            (string)$response->getBody(), true, fn($str, $def)
                => $this->servererror(new middlewareException(
                    "Bad auth-server response: $str.", 401
        )));

        if ($data == $this) return $this;

        $dict = [static::REFRESH_TOKEN];
        $rei = $data['refresh_expires_in'] ?? 60;
        $ei = $data['expires_in'] ?? 30;

        foreach ($data as $name => $val)
            $this->mc->add("keycloak_$name", $val, in_array("keycloak_$name", $dict) ? $rei : $ei);

        return controllerApi::$storage[static::ACCESS_TOKEN] = $data['access_token'] ?? null;
    }

    /**
     * Инициализирует HTTP-клиент
     *
     * @return $this
     */
    protected function initclient()
    {
        $this->loop = $this->loop ?? Factory::create();
        $this->client = $this->client ?? new Browser($this->loop);
        return $this;
    }
}