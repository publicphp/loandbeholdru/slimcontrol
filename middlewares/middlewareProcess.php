<?php


namespace loandbeholdru\slimcontrol\middlewares;


use loandbeholdru\slimcontrol\api\statuses;

class middlewareProcess extends middlewareBase
{
    public function __invoke($request, $response, $next = null)
    {
        $meth = [
            $request instanceof Slim\Http\Request ? $request->getOriginalMethod() : null,
            $request->getMethod()
        ];

        if (in_array('OPTIONS', $meth))
            parent::__invoke($request, $response, $next);

        $this->process($request, $response, $next);

        return parent::__invoke($request, $response, $next);
    }
    protected function process($request, $response, $next)
    {
        return $this->break(sprintf(
    'You have to implement process-method in class: %s', static::class
        ), statuses::ERROR, 503);
        return $this;
    }
}