<?php

namespace loandbeholdru\slimcontrol\middlewares;

use loandbeholdru\slimcontrol\api\controllerApi;
use loandbeholdru\slimcontrol\api\statuses;
use loandbeholdru\shorts\arrays;

/**
 * Рассчитывает, что nginx-proxy пришлет расшифрованный token в заголовке $header
 * Тогда будет оценена валидность и если токен валиден, его занчение будет размещено
 * controller::$storage[$key]
 *
 * Class decodeTokenMW
 * @package loandbeholdru\slimcontrol\middlewares
 */
class decodeTokenMW extends middlewareProcess
{
    static string $key;
    static string $header;
    /**
     * decodeTokenMW constructor.
     */
    public function __construct(string $key = 'token', string $headername = 'Application')
    {
        static::$key = strtolower($key);
        static::$header = ucfirst(strtolower($headername));
    }

    protected function process($request, $response, $next)
    {
        $headers = $request->getHeaders();
        $app = $headers[static::$header] ?? ['{}'];
        $data = arrays::valid_json(arrays::first($app));
        controllerApi::$storage[static::$key] =  $data;

        if (!($data['valid'] ?? false))
            $this->break("Can't find token data!", statuses::AUTH_FAIL, 401);

        return $this;
    }

}