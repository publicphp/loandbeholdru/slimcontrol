<?php


namespace loandbeholdru\slimcontrol\middlewares;



class allowOrigin extends middlewareBase
{
    const HEADERS = [
        'Accept', 'Accept-Language', 'Content-Language',
        'Content-Type', 'Origin', 'Authorization', 'Host'
    ];

    protected $headers;

    /**
     * allowOrigin constructor.
     * @param $headers
     */
    public function __construct(string ...$headers)
    {
        $this->headers = $headers ?? [];
    }


    public function __invoke($request, $response, $next = null)
    {

        $meth = $request->getMethod();
//        $origmeth = $request->getOriginalMethod();
        $headers = $request->getHeaders();

//        file_put_contents(sprintf('%s/%s.json',__DIR__ , uniqid()), json_encode(compact(
//            'meth','origmeth', 'headers'
//        )));

        $headers = array_merge(static::HEADERS, $this->headers);
//        $origin = $request->getHeader('Origin');
//        $origin = 'coin.1qr.org';

        return parent::__invoke($request, $response, $next)
            ->withAddedHeader('Access-Control-Allow-Origin',  empty($origin) ? '*' : $origin)
            ->withAddedHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH, OPTIONS')
            ->withAddedHeader('Access-Control-Expose-Headers', implode(', ', $headers))
            ->withAddedHeader('Access-Control-Allow-Headers', implode(', ', $headers));
    }

}