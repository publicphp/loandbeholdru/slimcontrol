<?php


namespace loandbeholdru\slimcontrol\middlewares;


use loandbeholdru\slimcontrol\controllers\controllerBase;


class sessionStorage extends middlewareBase
{
    protected $vars;

    /**
     * sessionStorage constructor.
     * @param $vars
     */
    public function __construct(string ...$vars)
    {
        $this->vars = $vars ?? [];
    }


    public function __invoke( $request,  $response, callable $next = null)
    {
        session_start();

        $response = parent::__invoke($request, $response, $next);

        foreach ($this->vars as $name) {
            controllerBase::$storage[$name] = $_SESSION[$name] ?? null;
            unset($_SESSION[$name]);
        }


        $response = $next($request, $response);

        foreach (controllerBase::$storage as $name => $value)
            $_SESSION[$name] = $value;

        return $response;
    }

}