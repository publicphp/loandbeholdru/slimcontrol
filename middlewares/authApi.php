<?php


namespace loandbeholdru\slimcontrol\middlewares;

use GuzzleHttp\Middleware;
use Middlewares\Utils\Factory\SlimFactory;
use Middlewares\Utils\Traits\HasResponseFactory;
use loandbeholdru\slimcontrol\api\controllerApi;
use loandbeholdru\slimcontrol\api\statuses;
use loandbeholdru\shorts\arrays;

use Slim\Handlers\Strategies\RequestHandler;
use Slim\Psr7\Response;

class authApi extends middlewareBase
{


    protected $headername;
    protected $options;
    /**
     * authApi constructor.
     */
    public function __construct(string $headername = 'Api-token', array $options = [])
    {
        $this->headername = $headername;
        $this->options = $options;

    }

    public function __invoke($request, $response, $next = null)
    {
        $meth = [
            $request instanceof Slim\Http\Request ? $request->getOriginalMethod() : null,
            $request->getMethod()
        ];

        if (in_array('OPTIONS', $meth))
            return parent::__invoke($request, $response, $next);

        try{
            controllerApi::$api_key = $this->checkAuth(
                [$this, 'authSource'], $request, $this->headername
            );

        }catch (authException $e){
            $this->break($e->getMessage(),statuses::AUTH_FAIL, 401);
        }catch (\Throwable $e){
            $this->break($e->getMessage(), statuses::ERROR, 501);
        }

        return parent::__invoke($request, $response, $next);
    }

    protected function checkAuth($exemplary, $request, string $headername)
    {
        $api_key = implode(' ', $request->getHeader($headername));
        $exemplary = is_callable($exemplary) ? $exemplary($api_key) : $exemplary;

        if (!$exemplary)
            throw new authException(
                arrays::ifDefined('', 'APP_DEBUG', false) ?
                    sprintf("Auth failed! Got: %s Expected: %s", $api_key, $exemplary) :
                    sprintf("Auth failed! Got bad key: %s", $api_key)
            );

        return $api_key;
    }

    protected function authSource(string $api_key = null)
    {
        return 'topsecret' === $api_key;
    }

}