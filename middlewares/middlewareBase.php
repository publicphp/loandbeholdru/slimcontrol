<?php


namespace loandbeholdru\slimcontrol\middlewares;



use loandbeholdru\slimcontrol\api\controllerApi;
use loandbeholdru\slimcontrol\api\statuses;
use loandbeholdru\shorts\classes;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Handlers\Strategies\RequestHandler;
use Slim\Psr7\Response;
use Slim\Routing\RouteRunner;


class middlewareBase
{


    public function __invoke( $request,  $response,  $next = null)
    {
        $havehandler = ($response instanceof RequestHandler) ||
            ($response instanceof RequestHandlerInterface) ||
            classes::is_anonymous($response);

        if ($havehandler){
            /** @var RequestHandler $response */
            $response = $response->handle($request);

        }else{
            $response = is_callable($next) ? $next($request, $response) : $response;
        }


        return $response;
    }

    /**
     * Стандартизатор огтветов
     *
     * @param $response
     * @param $message
     * @param string $status
     * @return \Slim\Psr7\Message|Response
     */
    public static function replyjson($response, $message, $status = statuses::SUCCESS)
    {
        if ($response instanceof Response){
            $response->getBody()
                ->write(json_encode(compact('status', 'message')));
        }else{
            $response = $response->withJson(compact('status', 'message'));
        }

        return  $response->withHeader('Content-Type', 'application/json');

    }

    /**
     * Вызывается, для установки сигнализации об ошибке самой middleware
     *
     * @param string $message
     * @param string $status
     * @param int $httpcode
     * @return $this
     */
    protected function break(string $message, string $status, int $httpcode)
    {
        $exception = new middlewareException($message, $httpcode);
        controllerApi::$break = controllerApi::$break
            ?? compact('status', 'exception');
        return $this;
    }

    /**
     * Функция стандартной обработки не 200-го ответа от сервера.
     * Используется для случаев, когда при подготовке работы контроллера
     * нужно сделать HTTPх запрос стороннему серверу.
     *
     * @param $reason
     * @return $this
     */
    public function servererror($reason)
    {
        if ($reason instanceof \Throwable){
            $msg =  $reason->getMessage();
            $code = $reason->getCode();
        }else{
            $msg =  json_encode($reason);
            $code = 500;
        }

        return $this->break($msg, statuses::ERROR, $code);
    }
}