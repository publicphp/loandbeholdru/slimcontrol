<?php


namespace loandbeholdru\slimcontrol\middlewares;




class testMiddleware extends middlewareBase
{
    protected $before;
    protected $after;

    /**
     * testMiddleware constructor.
     * @param $before
     * @param $after
     */
    public function __construct(string $before = '', string $after = '')
    {
        $this->before = $before;
        $this->after = $after;
    }


    public function __invoke( $request,  $response, callable $next = null)
    {
        $response = parent::__invoke($request, $response, $next);

        $response->getBody()->write($this->before);
        $response = $next($request, $response);
        $response->getBody()->write($this->after);

        return $response;
    }

}