<?php


namespace loandbeholdru\slimcontrol\middlewares;


use loandbeholdru\slimcontrol\api\statuses;
use loandbeholdru\shorts\arrays;


class dbConnectionException extends \Exception {};

class mysql extends middlewareBase
{
    protected $options;
    protected $configurator;

    /**
     * mysql constructor.
     * @param $options
     */
    public function __construct(array $options, string $configurator = null)
    {
        $this->options = $options;
        $this->configurator = $configurator ??
            arrays::ifDefined('', 'DB_CONFIGURATOR', null);
    }


    public function __invoke( $request,  $response,  $next = null)
    {

        $meth = [
            $request instanceof Slim\Http\Request ? $request->getOriginalMethod() : null,
            $request->getMethod()
        ];

        if (in_array('OPTIONS', $meth))
            parent::__invoke($request, $response, $next);

        try{
            if (!realpath($this->configurator))
                throw new dbConnectionException(
                    sprintf("Configurator not found! Path: %s", $this->configurator)
                );
            $db = require $this->configurator;
            $db($this->options);
        }catch (dbConnectionException $e){
            $this->break($e->getMessage(), statuses::DB_ERROR, 503);
        }catch (\Throwable $e){
            $this->break($e->getMessage(), statuses::DB_ERROR, 500);
        }

        return parent::__invoke($request, $response, $next);
    }

}