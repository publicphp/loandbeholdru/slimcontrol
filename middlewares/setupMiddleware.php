<?php


namespace loandbeholdru\slimcontrol\middlewares;


use loandbeholdru\shorts\arrays;


class setupMiddleware extends middlewareBase
{
    protected $proc;
    protected $fields;

    public function __construct(callable $proc, string ...$fields)
    {
        $this->proc = $proc;
        $this->fields = $fields;
    }

    public function __invoke( $request,  $response, callable $next = null)
    {
        $response = parent::__invoke($request, $response, $next);

        $proc = $this->proc;
        $attrs = $request->getAttributes();
        foreach ($this->fields as $field) 
            $params[$field] = arrays::findByKey( $field, $attrs['routeInfo'] ) ;
        
        $proc($params);
        
        return $response = $next($request, $response);
    }


}