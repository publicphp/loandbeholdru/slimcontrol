<?php


namespace loandbeholdru\slimcontrol\api;


use loandbeholdru\shorts\arrays;
use Psr\Container\ContainerInterface;

use Slim\Psr7\Request;
use Ulid\Ulid;

class baseApi
{
    const ANYTHING = 'anything from incoming';
    // Те параметры, которые пропускаем
    const IN = [];

    // Те параметры, которые контролируем. Если название ссылается на функцию,
    // то она вызывается ($key, $val)
    const ASSERT = [];

    // Те парамеры, которые устанавливаем по умолчанию. Если название ссылается на функцию,
    // то она вызывается ($key)
    const DEFAULTS = [];

    /** @var ContainerInterface */
    protected $container;
    /** @var Response */
    protected $response;
    /** @var Request */
    protected $request;

    protected $query;
    protected $body;
    protected $files;
    protected $args;
    protected $headers;

    /**
     * controllerBase constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;

    }


    public function __invoke( $request,  $response,  $args)
    {
        $this->request = $request;
        $this->response = $response;
        $this->headers = $request->getHeaders();

        $isFree = in_array(self::ANYTHING, $this->ins());
        $query = $request->getQueryParams() ?? [];

        $this->query =  $isFree ? $query : arrays::cutByKeys(
            arrays::lowkeys($query), ...$this->ins()
        );

        $body = $request->getParsedBody() ??
            arrays::valid_json((string)$request->getBody(), true, []);

        $this->body = $isFree ? $body : arrays::cutByKeys(
            arrays::lowkeys($body), ...$this->ins()
        );

        $files = $request->getUploadedFiles() ?? [];
        $this->files = $isFree ? $this->files : arrays::cutByKeys(
            arrays::lowkeys($files), ...$this->ins()
        );

        $this->args = ($this->body ?? []) + ($this->query ?? [])
            + ($this->files ?? []) + ($args ?? []);

        return $response->withStatus(200);
    }

    protected function defs()
    {
        return static::DEFAULTS;
    }

    protected function ins()
    {
        return static::IN;
    }

    protected function asss()
    {
        return static::ASSERT;
    }
}