<?php


namespace loandbeholdru\slimcontrol\api;


/**
 * СЕРВИС ЗАГОЛОВКОВ
 * Класс предполагает наличие глобальных задекларированных констант:
 *      KC_SCHEME, KC_SERVER, KC_CONTEXT
 *      KC_REALM, KC_USERNAME, KC_PASSWORD
 *      KC_CLIENT, KC_CLIENT_SECRET
 * Class headers
 * @package loandbeholdru\slimcontrol\api
 */
class headers
{
    const JSON = ['Content-Type' => 'application/json'];
    const FORM = ['Content-Type' => 'application/x-www-form-urlencoded'];

    const KC_CLIENT_CREDS = [
        "client_id" => KC_CLIENT, "client_secret" => KC_CLIENT_SECRET
    ];
    const KC_ADMIN_CREDS = [
        "username" => KC_USERNAME, "password" => KC_PASSWORD
    ];

    static function bearer(string $access_token, array ...$headers_list){
        $headers = [];
        foreach ($headers_list as $header)
            $headers = array_merge($headers, $header);

        $headers["Authorization"] = "Bearer $access_token";
        return $headers;
    }
}