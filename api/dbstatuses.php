<?php


namespace loandbeholdru\slimcontrol\api;


class dbstatuses
{
    const NEW_OPTION = 'suspect';
    const CHECKED = 'checked';
    const CONNECTED = 'connected';
    const NOT_FOUND = 'not_found';

    const DB_WRITE_ERROR = 'db_write_error';
    const DB_ERROR = 'db_unknown_error';
    const DB_CONNECTION_ERROR = 'db_connection_error';
    const CACHE_CONNECTION_ERROR = 'cache_connection_error';
    const CACHE_UNKNOWN_ERROR = 'cache_unknown_error';

}