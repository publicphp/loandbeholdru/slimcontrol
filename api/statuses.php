<?php


namespace loandbeholdru\slimcontrol\api;


class statuses
{
    const SUCCESS = 'success';
    const ERROR = 'error';
    const AUTH_FAIL = 'auth_fail';
    const DB_ERROR = 'db_error';
    const REQUEST_ERROR = 'request_error';
    const DISK_ERROR = 'disk_error';
}